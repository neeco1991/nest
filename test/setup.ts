import { rm } from 'fs/promises';
import { join } from 'path';
// import { getConnection } from 'typeorm';

global.beforeEach(async () => {
  // se il file non esiste la funzione lancia un errore quindi wrappo in try catch
  try {
    await rm(join(__dirname, '..', 'db.test.sqlite'));
  } catch (err) {}
});

// global.afterEach(async () => {
//   const conn = await getConnection();
//   await conn.close();
// });
