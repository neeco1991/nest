# Custom data serialization

## Excluding response properties

Quando un controller torna un istanza di una entity a chi effettua una chiamata API, questa istanza viene prima serializzata da oggetto a plain object.
Il decorator `@Exclude()` permette di escludere una particolare property di un'entità dal processo di serializzazione.

_user.entity.ts_

```ts
@Column()
@Exclude()
password: string;
```

Per fare in modo che questo funzioni, bisogna esplicitare nel controller che la risposta deve essere intercettata e serializzata con le regole definite nell'entity. In particolare:

_users.controller.ts_

```ts
@UseInterceptors(ClassSerializerInterceptor)
@Get('/:id')
async findUser(@Param('id') id: string) {
  [...]
}
```

In questo modo, abbiamo nascosto la password nella risposta.

## Solution to serialization

Cosa fare nel caso si voglia tornare cose diverse in base a se il chiamante è admin o meno?
Per esempio, si vuole tornare solo `id` ed `email` se il chiamante è un utente, ma si vuole tornare anche `age` e `name` se il chiamante è un admin.
Per fare ciò bisogna innanzitutto creare due endpoint diversi, per esempio `auth/:id` per gli utenti e `admin/auth/:id` per gli amministratori. Poi si deve costruire un _custom interceptor_ in cui vengono definite queste regole.
I DTO sono utilizzati non solo per serializzare i dati in ingresso ma anche quelli in uscita.

## How to build interceptors

Gli interceptors sono simili ai middleware. Essi intercettano le chiamate HTTP in ingresso e in uscita e le possono modificare.
Creo quindi una folder `src/interceptors` in cui inserisco un file `serialize.interceptor.ts`:

_serialize.interceptor.ts_

```ts
import {
  UseInterceptors,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { map, Observable } from 'rxjs';

export class SerializeInterceptor implements NestInterceptor {
  intercept(
    context: ExecutionContext,
    next: CallHandler<any>,
  ): Observable<any> | Promise<Observable<any>> {
    // qui codice per fare qualcosa con la incoming request
    return next.handle().pipe(
      map((data: any) => {
        // qui codice per fare qualcosa con la outgoing response
      }),
    );
  }
}
```

Per sostituire l'interceptor di default con questo custom:

_user.controller.ts_

```ts
@UseInterceptors(SerializeInterceptor)
@Get('/:id')
async findUser(@Param('id') id: string) {
  [...]
}
```

## Serialization in the interceptor

Per serializzare la risposta in uscita creo un nuovo DTO:

_user.dto.ts_

```ts
import { Expose } from 'class-transformer';

export class UserDto {
  @Expose()
  id: number;

  @Expose()
  email: string;
}
```

Quindi:

_serialize.interceptor.ts_

```ts
return next.handle().pipe(
  map((data: any) => {
    return plainToInstance(UserDto, data, {
      excludeExtraneousValues: true,
    });
  }),
);
```

In questo modo sto trasformando `data` in un JSON utilizzando `UserDto`. In `excludeExtraneousValues` posso specificare se voglio nascondere le coppie chiavi-valore che non sono definite nel DTO.

## Customizing the interceptor's DTO

Voglio fare in modo di utilizzare l'interceptor che ho creato anche per altre richieste che magari non hanno niente a che fare con gli users.
Per farlo devo definire un constructor nel serializer:

_serialize.interceptor.ts_

```ts
constructor(private dto: any) {}
```

Ora posso utilizzare l'interceptor definendo nel controller il tipo di dato che gli sto passando:

_users.controller.ts_

```ts
@UseInterceptors(new SerializeInterceptor(UserDto))
@Get('/:id')
async findUser(@Param('id') id: string) {
  [...]
}
```

## Wrapping the interceptor in a decorator

Per semplificare il codice, voglio creare un decorator che sostituisca la lunga riga `@UseInterceptors(new SerializeInterceptor(UserDto))` nel controller:

_serialize.interceptorr.ts_

```ts
export function Serialize(dto: any) {
  return UseInterceptors(new SerializeInterceptor(dto));
}
```

Quindi il controller diventa:

_user.controller.ts_

```ts
@Serialize(UserDto)
@Get('/:id')
async findUser(@Param('id') id: string) {
  [...]
}
```

## Controller-wide serialization

Per fare in modo che la serializzazione avvenga su tutti gli endpoint del controller, sposto il decoratore a livello di controller:

_user.controller.ts_

```ts
@Controller('auth')
@Serialize(UserDto)
export class UsersController {
  [...]
}
```

## A bit of type safety around serialize

Voglio eliminare più `any` possibile dal serializer. Il problema è che il serializer prende nel costruttore DTO diversi.
Per fare questo definisco un'interfaccia che identifica una generica classe:

_serialize.interceptor.ts_

```ts
interface ClassConstructor {
  new (...args: any[]): {};
}
```

E la applico al decorator che abbiamo appena creato:

_serialize.interceptor.ts_

```ts
export function Serialize(dto: ClassConstructor) {
  return UseInterceptors(new SerializeInterceptor(dto));
}
```

In questo modo se provo a chiamare il decorator con una stringa ottengo un errore.

# Authentication from scratch

## Authentication overview

Quando viene interrogato `auth/signup` con `email` e `password`, torniamo al client un cookie che contiene l'user id. Ogni volta che l'utente fa una nuova request, ci allega il cookie. In questo modo sappiamo chi è l'utente a fare la request.

## Understanding password hashing

Due cose importanti bisogna ricordare delle funzioni di hash:

1. Una piccola modifica all'input, porta ad un output completamente diverso
2. Una volta applicata la funzione di hash, non è più possibile tornare al testo originale

Quando facciamo il signup, la password passa attraverso una funzione di hash e viene salvata sul DB criptata. Ogni volta che un utente fa signup fornendo una password, la password viene passata nuovamente attraverso la funzione di hash e il risultato viene confrontato con quello che c'è a DB. In questo modo, anche se Bob è a conoscenza di tutte le password salvate a DB, non è in grado di effettuare un login.

Il rainbow table attack consiste nell'applicare la funzione di hash alle password più frequenti e poi cercare i match nelle password hashate a DB. Per evitare che questo accada, creiamo una piccola stringa (chiamata `salt`) che aggiungiamo sempre alle password degli utenti. Ad ogni utente viene aggiunto il _salt_ generato randomicamente. La concat viene hashata e il valora che viene salvato su DB è il risultato di questo hash, concatenato con `.salt` (quindi il salt viene replicato in chiaro). In questo modo le password salvate a DB non saranno mai tra quelle più frequenti. Le password così create si chiamano _hashed and salted_. In fase di signin viene presa la password in input, concatenata con il salt a DB, hashata e riconcatenata con `.salt`. Se la stringa ottenuta è uguale a quella a DB, l'utente viene autenticato. In questo modo, Bob dovrebbe crearsi una rainbow table apposta per ogni utente, il che risulterebbe estremamente complesso.

## Salting and hashing the password

_auth.service.ts_

```ts
async signup(email: string, password: string) {
  const alreadyExists = (await this.usersService.find(email)).length > 0;
  if (alreadyExists) {
    throw new Error('Email already exists');
  }

  // salting and hashing
  const salt = randomBytes(8).toString('hex');
  // voglio un hash di lunghezza 32
  // ts non conosce il tipo ritornato da scrypt quindi lo espicito
  const hash = (await scrypt(password, salt, 32)) as Buffer;
  // concatenazione di salt + hash
  const hashedPassword = `${salt}.${hash.toString('hex')}`;

  [...]
}
```

Nel DB vedo una password del genere:
6999c178a68bafeb.f89a86bd6267879e2b254ac6921ad80b906bfab01ce77c5c95adffbe663a5204

dove la prima parte è il `salt` e la seconda l'`hash`.

## Handling user signin

_auth.service.ts_

```ts
async signin(email: string, password: string) {
  const [user] = await this.usersService.find(email);
  if (!user) {
    throw new Error('User not found');
  }

  const [salt, storedHash] = user.password.split('.');

  const hash = (await scrypt(password, salt, 32)) as Buffer;

  if (storedHash !== hash.toString('hex')) {
    throw new Error('Bad password');
  }
  return user;
}
```

_users.controller.ts_

```ts
constructor(
  private usersService: UsersService,
  private authService: AuthService,
) {}

@Post('/signup')
async createUser(@Body() body: CreateUserDto) {
  try {
    return await this.authService.signup(body.email, body.password);
  } catch (err) {
    switch (err.message) {
      case 'Email already exists':
        throw new BadRequestException('Email already exists');
    }
  }
}

@Post('/signin')
async signin(@Body() body: CreateUserDto) {
  try {
    return await this.authService.signin(body.email, body.password);
  } catch (err) {
    switch (err.message) {
      case 'Bad password':
        throw new BadRequestException('Bad password');
      case 'User not found':
        throw new NotFoundException('User not found');
    }
  }
}
```

## Setting up sessions

Il package che usiamo per la gestione dei coockie è `cookie-session`.
Quando il client attacca il cookie alla request, mette una stringa chiamata `Cookie` negli headers.
Questa stringa contiene delle informazioni che decodificate diventano un JSON (tipo JWT). Lato server accediamo alla sessione utilizzando un decorator. Quindi possiamo aggiungere/rimuovere/modificare proprietà di questo JSON. Il JSON viene ricodificato e la response conterrà un header `Set-Cookie` con la nuova stringa.

`npm install cookie-session @types/cookie-session`

_main.ts_

```ts
import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
// facciamo un import così perché typescript ha qualche problema con questa libreria
const cookieSession = require('cookie-session');

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(
    cookieSession({
      // lista delle chiavi che conterrà il nostro cookie
      keys: ['test'],
    }),
  );
  app.useGlobalPipes(new ValidationPipe({ whitelist: true }));
  await app.listen(3000);
}
bootstrap();
```

## Changing and fetching session data

_user.controller.ts_

```ts
@Get('/colors/:color')
setColor(@Param('color') color: string, @Session() session: any) {
  session.color = color;
}

@Get('/colors')
getColor(@Session() session: any) {
  return session.color;
}
```

In `/colors/:color` settiamo la chiave `color` con quello che troviamo nei parametri. La libreria pensa ad inserire autonomamente la chiave `Set-Cookie` nella response quando cambiamo la sessione. A questo punto se chiamiamo `/colors`, otteniamo l'ultimo colore che gli abbiamo inviato.

Ora elimino questi route handler perchè erano solo di esempio.

## Signin in a user

Salviamo l'id dello utente nei cookie:

_users.controller.ts_

```ts
@Post('/signup')
async createUser(@Body() body: CreateUserDto, @Session() session: any) {
  try {
    const user = await this.authService.signup(body.email, body.password);
    session.userId = user.id;
    return user;
  } catch (err) {
    [...]
    }
  }
}

@Post('/signin')
async signin(@Body() body: CreateUserDto, @Session() session: any) {
  try {
    const user = await this.authService.signin(body.email, body.password);
    session.userId = user.id;
    return user;
  } catch (err) {
    [...]
    }
  }
}

```

Se faccio signup e signin, la seconda volta non avrò un header `Set-Cookie` nella risposta perché la libreria si rende conto che l'id non è cambiato.

## Getting the current user

Se lancio questo dopo il signin ottengo l'utente:

_users.controlle.ts_

```ts
@Get('/whoami')
whoAmI(@Session() session: any) {
  return this.usersService.findOne(session.userId);
}
```

## Signin out a user

_users.controller.ts_

```ts
@Post('/signout')
signOut(@Session() session: any) {
  session.userId = null;
}
```

Se faccio solo così ottengo comunque l'utente usando whoami. Questo perché il metono `findOne` dentro whoami, se viene chiamato con argomento `null`, torna il primo utente nel db. Quindi modifico questo metodo:

_users.service.ts_

```ts
findOne(id: number) {
  if (!id) {
    return null;
  }
  return this.repo.findOne({ where: { id } });
}
```

Ora il signout funziona.

## Two automation tools

Abbiamo bisogno di un tool per dire ad un endpoint se l'utente è loggato e uno per dire chi è il current signed user.
Per il primo problema costruiremo una guard, per il secondo un interceptor + un decorator.

## Custom param decoratos

Voglio fare un decorator del genere:

_users.controller.ts_

```ts
@Get('whoami')
whoAmI(@CurrentUser() user: User) {}
```

Quindi creo una folder `decorators` dentro users e all'interno creo un file `current-user.decorator.ts`.

_current-user.decorator.ts_

```ts
import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const CurrentUser = createParamDecorator(
  // Usiamo ExecutionContext invece di request (anche se è la incoming request) perché
  // almeno funziona anche per le websockets etc.
  (data: any, context: ExecutionContext) => {
    return 'test';
  },
);
```

In questo modo se mettiamo:

_users.controller.ts_

```ts
@Get('whoami')
whoAmI(@CurrentUser() user: string) {
  return user;
}
```

Quando facciamo una request ci risponde 'test'.

## Why a decorator and interceptor

L'argomento `data` che prende in input la funzione definita in `CurrentUser` è lo stesso che viene passato all'interno del decorator `@CurrentUser()` nell'endpoint del controller.
Siccome non voglio argomenti posso definirgli il tipo `never`:

_current-user.decorator.ts_

```ts
export const CurrentUser = createParamDecorator(
  (data: never, context: ExecutionContext) => {
    return 'test';
  },
);
```

In questo modo, se provo a passare argomenti al decorator nel controller, TypeScript mi da un errore.

All'interno del decorator però abbiamo bisogno della sessione per recuperare l'id dell'utente. Inoltre abbiamo bisogno anche dell'user service per recuperare l'utente a partire dall'id.

Il `context` è un wrapper attorno alla incoming request.

_current-user.decorator.ts_

```ts
export const CurrentUser = createParamDecorator(
  (data: never, context: ExecutionContext) => {
    const request = context.switchToHttp().getRequest();
    // posso recuperare l'id da request.session.userId;
    return 'test';
  },
);
```

Mi manca da usare l'user service dentro il decorator. Il problema è che non mi basta importarlo dentro il decorator perché i servizi sono creati tramite dependency injection. Non possiamo usare la DI con i decorator. Il workaround è quello di creare un interceptor.

Questo interceptor sarà in grado di vedere il session object e, tramite DI, l'user service. Quindi l'interceptor esporrà l'utente al decorator.

## Communicating from interceptor to decorator

Creiamo un nuovo interceptor dentro `users/interceptors` chiamato `current-user.interceptor.ts`.

_current-user.interceptor.ts_

```ts
import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { UsersService } from '../users.service';

@Injectable()
export class CurrentUserInterceptor implements NestInterceptor {
  constructor(private userService: UsersService) {}

  async intercept(
    context: ExecutionContext,
    next: CallHandler<any>,
  ): Promise<Observable<any>> {
    const request = context.switchToHttp().getRequest();
    const { userId } = request.session || {};

    if (userId) {
      const user = await this.userService.findOne(userId);

      // attacco l'utente alla request di modo che sia poi visibile dal decorator
      request.currentUser = user;
    }

    // vai avanti ed esegui il route handler
    return next.handle();
  }
}
```

Ora abbiamo l'utente dentro il campo `currentUser` della sessione e lo possiamo recuperare dal decorator:

_current-user.decorator.ts_

```ts
export const CurrentUser = createParamDecorator(
  (data: never, context: ExecutionContext) => {
    const request = context.switchToHttp().getRequest();
    return request.currentUser;
  },
);
```

È possibile fare tutto questo usando solo l'interceptor, il decorator mi è comodo perché almeno non devo andare a scrivere `request.currentUser` nel controller ma me lo trovo già nella variabile assegnata a `@CurrentUser()`.

## Connecting an interceptor to dependency injection

Per rendere l'interceptor injectable, lo aggiungo alla lista dei providers di `users.module.ts`.

Ora lo posso aggiungere al controller, di modo che venga eseguito prima di ogni route handler:

_users.controller.ts_

```ts
@Controller('auth')
@Serialize(UserDto)
@UseInterceptors(CurrentUserInterceptor)
export class UsersController {
  [...]
}
```

## Globally scoped interceptors

L'interceptor appena creato è applicato ad un sigolo controller. Cosa fare se lo vigliamo applicare a tutti i controller?
Se vogliamo evitare di usare `@UseInterceptors(CurrentUserInterceptor)` su tutti i controller, possiamo fare un globally scoped interceptor.

Eliminiamo `@UseInterceptors(CurrentUserInterceptor)` dal controller. Poi vado nel module:

_users.module.ts_

```ts
import { APP_INTERCEPTOR } from '@nestjs/core';

@Module({
  imports: [TypeOrmModule.forFeature([User])],
  controllers: [UsersController],
  providers: [
    UsersService,
    AuthService,
    { provide: APP_INTERCEPTOR, useClass: CurrentUserInterceptor },
  ],
})
export class UsersModule {}
```

Il downside è che eseguo l'interceptor anche per i controller a cui non serve l'utente.

## Preventing access with authentication guard

Anche le guardie possono essere applicate o globalmente, o a livello di controller, oppure a livello di route handler. Creo il file `guards/auth.guard.ts`:

_auth.guard.ts_

```ts
import { CanActivate, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';

export class AuthGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();

    return request.session.userId;
  }
}
```

Ora mi basta usare `@UseGuards(AuthGuard)` per eseguirla prima di quello che voglio.
In questo modo se faccio una request e non sono loggato, ottengo:

```JSON
{
  "statusCode": 403,
  "message": "Forbidden resource",
  "error": "Forbidden"
}
```

# Getting started with Unit Testing

## Testing overview

Gli unit testing ci permettono di testare individualmente i metodi delle classi.
L'e2e (end to end o integration testing) ci permettono invece di testare l'intero flow di una feature => facendo una request ottengo la response che mi aspetto.

Il file `/test/app.e2e-spec.ts` è un esempio di e2e test. Gli unit test vanno invece nella directory `/src`, ad esempio il file `/src/users/users.controller.spec.ts` serve a testare _users.controller.ts_.

Utilizzeremo la DI per non dover creare un nuovo user service e un nuovo user repo. Creeremo un testing DI container.

## Testing setup

Creo il file `/src/auth.service.spec.ts` per testare l'auth service che abbiamo creato.

_auth.service.spec.ts_

```ts
import { Test } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { UsersService } from './users.service';

it('can create an instance of auth service', async () => {
  // creo il DI container
  const module = await Test.createTestingModule({
    providers: [AuthService],
  }).compile();

  const service = module.get(AuthService);

  expect(service).toBeDefined();
});
```

Quindi lancio il comando `npm run test:watch`.
Il test fallisce perché non riesce a creare `AuthService` dal momento che non gli abbiamo dato tutte le dipendenze di cui ha bisogno.

Faccio le seguenti modifiche per fornirgli `UserService` come dipendenza:

_auth.service.spec.ts_

```ts
it('can create an instance of auth service', async () => {
  // create a fake copy of the user service
  const fakeUserService = {
    find: () => Promise.resolve([]),
    create: (email: string, password: string) =>
      Promise.resolve({ id: 1, email, password }),
  };

  // creo il DI container
  const module = await Test.createTestingModule({
    providers: [
      AuthService,
      { provide: UsersService, useValue: fakeUserService },
    ],
  }).compile();

  const service = module.get(AuthService);

  expect(service).toBeDefined();
});
```

E ora funziona! Perché?

## Yes, testing is confusing

Il secondo parametro di providers mi dice che ogni volta che qualcuno chiederà un'istanza di `UserService`, di fornirgli `fakeUsersService`.

Quindi `AuthService` chiede un'istanza di `UsersService` al DI container. Il DI container gli risponde fornendogli l'oggetto `{ find, create }`. Possiamo immaginare che l'oggetto sia un'istanza dello `UsersService`. Questo vuol dire che se chiamiamo la `find` o la `create`, vengono eseguite le funzioni definite nell'oggetto.

## Getting TypeScript to help with mocks

Posso aggiungere una type notation a `fakeUserService` per essere aiutato a fornire delle callback compatibili con quelle che torna `UserService`:

_auth.service.spec.ts_

```ts
const fakeUserService: Partial<UsersService> = {
  find: () => Promise.resolve([]),
  create: (email: string, password: string) =>
    Promise.resolve({ id: 1, email, password }),
};
```

Così facendo però TypeScript si incazza perché `{ id: 1, email, password }` non è un `User` =>

_auth.service.spec.ts_

```ts
const fakeUserService: Partial<UsersService> = {
  find: () => Promise.resolve([]),
  create: (email: string, password: string) =>
    Promise.resolve({ id: 1, email, password } as User),
};
```

## Quick note to help speed up your tests

Cambiare `"test:watch": "jest --watch"` nel `package.json` con `"test:watch": "jest --watch --maxWorkers=1"` per aumentare la velocità di esecuzione.

## Imporving file layout

Faccio del refactoring per non dover ripetere il pezzo di codice che crea il servizio in tutti i test

_auth.service.spec.ts_

```ts
import { Test } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { User } from './user.entity';
import { UsersService } from './users.service';

describe('AuthService', () => {
  let service: AuthService;

  beforeEach(async () => {
    // create a fake copy of the user service
    const fakeUserService: Partial<UsersService> = {
      find: () => Promise.resolve([]),
      create: (email: string, password: string) =>
        Promise.resolve({ id: 1, email, password } as User),
    };

    // creo il DI container
    const module = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: UsersService,
          useValue: fakeUserService,
        },
      ],
    }).compile();

    service = module.get(AuthService);
  });

  it('can create an instance of auth service', async () => {
    expect(service).toBeDefined();
  });
});
```

Describe aggiunge solo la scritta `AuthService` alla console per organizzare meglio l'output.

## Ensuring password gets hashed

_auth.service.spec.ts_

```ts
it('creates a new users with a salted and hashed password', async () => {
  const password = 'testtest';
  const user = await service.signup('test@test.com', password);
  expect(user.password).not.toEqual(password);

  const [salt, hash] = user.password.split('.');
  expect(salt).toBeDefined();
  expect(hash).toBeDefined();
});
```

## Changing mock implementations

Vogliamo creare un test che verifichi che venga lanciato un errore quando cerchiamo di fare signup con una mail già esistente.

Il problema è che abbiamo già definito `fakeUserService` per farci tornare un array vuoto quando chiamiamo la funzione `find()`. Per risolvere mi basta dichiarare `fakeUserService` esternamente al `beforeEach()` e modificarlo nel test che deve testare l'eccezione:

_auth.service.spec.ts_

```ts
let service: AuthService;
let fakeUserService: Partial<UsersService>;

beforeEach(async () => {
  // create a fake copy of the user service
  const fakeUserService = {
    find: () => Promise.resolve([]),
    create: (email: string, password: string) =>
      Promise.resolve({ id: 1, email, password } as User),
  };
  [...]
})

[...]

it('throws an error if user signs up with email that is in use', async () => {
  fakeUserService.find = () =>
    Promise.resolve([
      { id: 1, email: 'test@test.com', password: 'testtest' } as User,
    ]);
  // const userPromise = service.signup('test@test.com', 'testtest');
  await expect(service.signup('test@test.com', 'testtest')).rejects.toThrow(
    'Email already exists',
  );
});
```

## Testing the signin flow

Possiamo testare che anghe il signin lanci un errore se utilizziamo un'email non esistente:

_auth.service.spec.ts_

```ts
it('throws if signin is called with an unused email', async () => {
  const userPromise = service.signin('test@test.conm', 'testest');
  await expect(userPromise).rejects.toThrow('User not found');
});
```

## Checking password comparison

Faccio un test per verificare che con password sbagliata ho un errore:

_auth.service.spec.ts_

```ts
it('throws if invalid password is provided', async () => {
  fakeUserService.find = () =>
    Promise.resolve([
      { email: 'test@test.com', password: 'test', id: 1 } as User,
    ]);

  const userPromise = service.signin('test@test.com', 'not-test');
  await expect(userPromise).rejects.toThrow('Bad password');
});
```

E che il login funzioni con la corretta password

_auth.service.spec.ts_

```ts
it('returns an user if correct password is provided', async () => {
  fakeUserService.find = () =>
    Promise.resolve([
      { email: 'test@test.com', password: 'test', id: 1 } as User,
    ]);

  const user = await service.signin('test@test.com', 'test');
  expect(user).toBeDefined();
});
```

Il problema qui è che la find dovrebbe tornare la password corretta hashed and salted, cosa che non sta accadendo, quindi il test non passa. Quindi sono costretto a loggare il signup con quella password per ottenere il risultato hashed and salted. In questo caso:

_auth.service.spec.ts_

```ts
it('returns an user if correct password is provided', async () => {
  fakeUserService.find = () =>
    Promise.resolve([
      {
        email: 'test@test.com',
        password:
          'dfff0697f221c283.73871a49104d69f72fb3190c10ad16d3c02d8e738a7c8821ef04a9a632f14745',
        id: 1,
      } as User,
    ]);

  const user = await service.signin('test@test.com', 'test');
  expect(user).toBeDefined();
});
```

Il problema rimane che se qualcuno mai cambierà quella stringa non funzionerà più.

## More intelligent mocks

Troviamo un modo per non dover loggare la password e schiantarla nel test.
Al posto di avere `find` e `create` mockate così, posso fare diversamente. Se funzionassero come funzionano nell'app (quindi se salvassero veramente l'utente) posso fare in modo di fare signup e poi signin con la giusta password.

Una soluzione è quella di creare un array di utenti e tenerlo in RAM:

_auth.service.spec.ts_

```ts
// create a fake copy of the user service
const users: User[] = [];
fakeUserService = {
  find: (email: string) => {
    const filteredUsers = users.filter((user) => user.email === email);
    return Promise.resolve(filteredUsers);
  },
  create: (email: string, password: string) => {
    const user = {
      email,
      password,
      id: Math.floor(Math.random() * 999999),
    } as User;
    users.push(user);
    return Promise.resolve(user);
  },
};
```

Quindi il mio test diventa:

_auth.service.spec.ts_

```ts
it('returns an user if correct password is provided', async () => {
  await service.signup('test@test.com', 'test');

  const user = await service.signin('test@test.com', 'test');
  expect(user).toBeDefined();
});
```

Nei casi più semplici non serve creare dei mock così complessi. Meglio crearli solo quando si è costretti.

## Refactoring to use intelligent mocks

Con questo cambio posso modificare alcuni precedenti test:

_auth.service.spec.ts_

```ts
it('throws an error if user signs up with email that is in use', async () => {
  await service.signup('test@test.com', 'testtest');

  const userPromise = service.signup('test@test.com', 'testtest');
  await expect(userPromise).rejects.toThrow('Email already exists');
});

it('throws if invalid password is provided', async () => {
  await service.signup('test@test.com', 'test');

  const userPromise = service.signin('test@test.com', 'not-test');
  await expect(userPromise).rejects.toThrow('Bad password');
});
```

## Unit testing a controller

Inizializzo `users.controller.spec.ts`:

_users.controller.spec.ts_

```ts
import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { User } from './user.entity';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';

describe('UsersController', () => {
  let controller: UsersController;
  let fakeUserService: Partial<UsersService>;
  let fakeAuthService: Partial<AuthService>;

  beforeEach(async () => {
    fakeUserService = {
      findOne: () => {},
      find: () => {},
      remove: () => {},
      update: () => {},
    };
    fakeAuthService = {
      signup: () => {},
      signin: () => {},
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        { provide: UsersService, useValue: fakeUserService },
        { provide: AuthService, useValue: fakeAuthService },
      ],
    }).compile();

    controller = module.get<UsersController>(UsersController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
```

## More mock implementations

Per ora faccio solo il mock delle cose che mi servono per i route handler che voglio testare:

_users.controller.spec.ts_

```ts
fakeUserService = {
  findOne: (id: number) => {
    return Promise.resolve({
      id,
      email: 'test@test.com',
      password: 'test',
    } as User);
  },
  find: (email: string) => {
    return Promise.resolve([
      {
        id: 1,
        email,
        password: 'test',
      } as User,
    ]);
  },
  // remove: () => {},
  // update: () => {},
};
fakeAuthService = {
  // signup: () => {},
  // signin: () => {},
};
```

## Not super effective tests

Remember: non possiamo testare i decorator negli UT ma dovremmo usare e2e.
Possiamo testare `findAllUsers` e `findUser` che usano la `find` e la `findOne` che abbiamo mockato.

_user.controller.spec.ts_

```ts
it('findAllUsers returns a list of users with the given email', async () => {
  const users = await controller.findAllUsers('test@test.com');
  expect(users.length).toEqual(1);
  expect(users[0].email).toEqual('test@test.com');
});

it('findUser returns a signle user with the given id', async () => {
  const user = await controller.findUser('1');
  expect(user).toBeDefined();
});

it('findUser throws an error if user with gicen id is not found', async () => {
  fakeUserService.findOne = () => null;
  await expect(controller.findUser('1')).rejects.toBeInstanceOf(
    NotFoundException,
  );
});
```

## Testing the signin method

Cambiamo il mock di `AuthService`:

_users.controller.spec.ts_

```ts
fakeAuthService = {
  // signup: () => {},
  signin: (email: string, password: string) => {
    return Promise.resolve({ id: 1, email, password } as User);
  },
};
```

Ora possiamo fare il test:

_users.controller.spec.ts_

```ts
it('signin updates session obkect and returns user', async () => {
  const session = { userId: -100 };
  const user = await controller.signin(
    { email: 'test@test.com', password: 'test' },
    session,
  );

  expect(user.id).toEqual(1);
  expect(session.userId).toEqual(1);
});
```

# Integration testing

## Getting started with end to end testing

Il file `test/app.e23-spec.ts` contiene un esempio di test e2e.
I test e2e partono creando un'intera istanza dell'applicazione. Ogni test crea una nuova istanza dell'applicazione.

Per lanciare i test e2e, eseguiamo nel teminale:

```sh
npm run test:e2e
```

## Creating an end to end test

Creo il nuovo file di test `test/auth.e2e-spec.ts`

```ts
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('Authentication system', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('handles a signup request', () => {
    const userEmail = 'test@test.com';
    return request(app.getHttpServer())
      .post('/auth/signup')
      .send({ email: userEmail, password: 'test' })
      .expect(201)
      .then((response) => {
        const { id, email } = response.body;
        expect(id).toBeDefined();
        expect(email).toEqual(userEmail);
      });
  });
});
```

Il test così però fallisce perché l'id non è definito. Perché?

## App setup issues in spec files

Durante il testing, il codice dentro `src/main.ts` non viene eseguito. Invece di eseguire quel file, il test e2e importa direttamente `AppModule` nel `beforEach` e crea l'app a partire da quel modulo.
Una soluzione possibile è quella di fare il setup di `coockieSession` e `useGlobalPipes` utilizzando le stesse funzioni di `main.ts`, ma non è la _nest way_.

## Applying globally scoped pipe

Nest vorrebbe che la validation pipe e la cookie session siano definite dentro l'`AppModule`. Cominciamo a spostare la validation pipe:

_app.module.ts_

```ts
import { Module, ValidationPipe } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { ReportsModule } from './reports/reports.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './users/user.entity';
import { Report } from './reports/report.entity';
import { APP_PIPE } from '@nestjs/core';

@Module({
  imports: [
    UsersModule,
    ReportsModule,
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'db.sqlite',
      entities: [User, Report],
      synchronize: true,
    }),
  ],
  controllers: [AppController],
  providers: [
    AppService,
    // ogni richiesta che arriva all'app deve passare attraverso la ValidationPipe
    { provide: APP_PIPE, useValue: new ValidationPipe({ whitelist: true }) },
  ],
})
export class AppModule {}
```

A questo punto posso eliminare lo statement di creazione della `ValidationPipe` da `main.ts`.

## Applying a globally scoped middleware

Per importare anche `cookieSession` a livello di `AppModule`:

_app.module.ts_

```ts
[...]
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(
        cookieSession({
          keys: ['test'],
        }),
      )
      .forRoutes('*'); // per tutte le routes
  }
}
```

Il mio file `main.ts` diventa:

_main.ts_

```ts
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(3000);
}
bootstrap();
```

In questo modo è un po' più difficile vedere quali sono le funzioni che vengono eseguite per tutte le rotte, però non devo configurare diversamente i test.

I test in questo modo funzionano, però non 2 volte di fila perché l'user viene creato davvero, quindi la seconda volta che faccio il test ottengo _email already exists_.

## Solving failures around repeat test runs

Una soluzione possibile sarebbe quella di randomizzare l'email ma ci troveremo il DB pieno di schifezze. La soluzione è quella di wipare il DB e ricrearlo ad ogni test. Però dobbiamo creare un altro DB, altrimenti andiamo a wipare dati che ci possono servire.

## Creating separate test and dev databases

Per fare questo dobbiamo comprendere con nest utilizza le variabili d'ambiente. In particolare, nest utilizza un modulo che si chiama `ConfigService` che configura tutta l'app.

# Managing app configuration

## Understanding dotenv

Lancio nel terminale:

```sh
npm install @nestjs/config
```

Questo pacchetto contiene internalmente `dotenv`. La libreria `dotenv` consiglia di creare solo un file `.env` mentre nest consiglia di crearne uno per ogni ambiente. Seguiamo nest:

## Applying dotenv for config

Creo due `.env`

_env.development_

```env
DB_NAME=db.sqlite
```

_env.test_

```env
DB_NAME=db.test.sqlite
```

Quindi in `AppModule` faccio il refactoring degli imports:

_app.module.ts_

```ts
imports: [
  ConfigModule.forRoot({
    // non voglio importarlo anche negli altri moduli
    isGlobal: true,
    // scelgo il file delle variabili d'ambiente
    envFilePath: `.env.${process.env.NODE_ENV}`,
  }),
  UsersModule,
  ReportsModule,
  TypeOrmModule.forRootAsync({
    // vai a prendere il CofigService che contiene le configurazioni
    inject: [ConfigService],
    useFactory: (config: ConfigService) => ({
      type: 'sqlite',
      database: config.get<string>('DB_NAME'),
      synchronize: true,
      entities: [User, Report],
    }),
  }),
],
```

## Specifying the runtime environment

Ci manca da specificare `NODE_ENV` nei vari ambienti. Per fare questo installiamo il pacchetto `cross-env` e ci rechiamo nel `package.json`:

_package.json_

```JSON
"scripts": {
    "prebuild": "rimraf dist",
    "build": "nest build",
    "format": "prettier --write \"src/**/*.ts\" \"test/**/*.ts\"",
    "start": "cross-env NODE_ENV=development nest start",
    "start:dev": "cross-env NODE_ENV=development nest start --watch",
    "start:debug": "cross-env NODE_ENV=development nest start --debug --watch",
    "start:prod": "node dist/main",
    "lint": "eslint \"{src,apps,libs,test}/**/*.ts\" --fix",
    "test": "cross-env NODE_ENV=test jest",
    "test:watch": "cross-env NODE_ENV=test jest --watch --maxWorkers=1",
    "test:cov": "cross-env NODE_ENV=test jest --coverage",
    "test:debug": "cross-env NODE_ENV=test node --inspect-brk -r tsconfig-paths/register -r ts-node/register node_modules/.bin/jest --runInBand",
    "test:e2e": "cross-env NODE_ENV=test jest --config ./test/jest-e2e.json"
  },
```

Infine:

_.gitignore_

```gitignore
.env.development
.env.test
```

## Solving SQLite error

Lui ottiene un errore _unable to connect to database_.
Il motivo perché a lui appare l'errore è che ci sono 2 test e2e che creano contemporaneamente 2 app che utilizzano lo stesso DB. Per cambiare il comporamento di Jest:

_package.json_

```JSON
"scripts" : {
  [...]
  "test:e2e": "cross-env NODE_ENV=test jest --config ./test/jest-e2e.json --maxWorkers=1"
}
```

## It works!

Dobbiamo wipare il DB prima di eseguire i test. Per fare questo eliminiamo e ricreiamo il file `db.test.sqlite` prima di ogni test. Dobbiamo quindi creare un `beforEach` globale che eseguono tutti i test:

_jest-e2e.json_

```JSON
{
  [...]
  "setupFilesAfterEnv": ["<rootDir>/setup.ts"]
}
```

Quindi creiamo il file `test/setup.ts`:

_setup.ts_

```ts
import { rm } from 'fs/promises';
import { join } from 'path';

global.beforeEach(async () => {
  // se il file non esiste la funzione lancia un errore quindi wrappo in try catch
  try {
    await rm(join(__dirname, '..', 'db.test.sqlite'));
  } catch (err) {}
});
```

## A followup test

Facciamo ora un test di signup e retrieve dell'utente loggato:

_auth.e2e-spec.ts_

```ts
it('signup as a new user then get the currently logged user', async () => {
  const userEmail = 'test@test.com';
  const response = await request(app.getHttpServer())
    .post('/auth/signup')
    .send({ email: userEmail, password: 'test' })
    .expect(201);

  const cookie = response.get('Set-Cookie');

  const { body } = await request(app.getHttpServer())
    .get('/auth/whoami')
    .set('Cookie', cookie)
    .expect(200);

  expect(body.email).toEqual(userEmail);
});
```

# Relations with TypeORM

## Adding properties to reports

Creiamo l'entità per i reports:

_report.entity.ts_

```ts
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Report {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  price: number;

  // company who makes the veichle
  @Column()
  make: string;

  @Column()
  model: string;

  @Column()
  year: number;

  // longitude
  @Column()
  lng: number;

  // latitude
  @Column()
  lat: number;

  @Column()
  milage: number;
}
```

## A DTO for report creation

Creiamo una nuova route per creare un report:

_reports.controller.ts_

```ts
import { Body, Controller, Post } from '@nestjs/common';
import { CreateReportDto } from './dtos/create-report.dto';

@Controller('reports')
export class ReportsController {
  @Post()
  createReport(@Body() body: CreateReportDto) {}
}
```

Quindi il `CreateReportDto`:

_create-report.dto.ts_

```ts
export class CreateReportDto {
  make: string;

  model: string;

  year: number;

  mileage: number;

  lng: number;

  lat: number;

  price: number;
}
```

## Receiving report creation request

Aggiungo le tipizzazioni al DTO:

_create-report.dto.ts_

```ts
import {
  IsLatitude,
  IsLongitude,
  IsNumber,
  IsString,
  Max,
  Min,
} from 'class-validator';

export class CreateReportDto {
  @IsString()
  make: string;

  @IsString()
  model: string;

  @IsNumber()
  @Min(1900)
  @Max(2050)
  year: number;

  @IsNumber()
  @Min(0)
  @Max(1000000)
  mileage: number;

  @IsLongitude()
  lng: number;

  @IsLatitude()
  lat: number;

  @IsNumber()
  @Min(0)
  @Max(1000000)
  price: number;
}
```

Configuro il servizio e creo il metodo per creare un nuovo report:

_reports.service.ts_

```ts
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateReportDto } from './dtos/create-report.dto';
import { Report } from './report.entity';

@Injectable()
export class ReportsService {
  constructor(@InjectRepository(Report) private repo: Repository<Report>) {}

  create(reportDto: CreateReportDto) {
    const report = this.repo.create(reportDto);
    return this.repo.save(report);
  }
}
```

Infine nel controller faccio la chiamata al servizio:

_reports.controller.ts_

```ts
@Post()
createReport(@Body() body: CreateReportDto) {
  return this.reportsService.create(body);
}
```

## Testing report creation

Questa volta creaiamo il file `request.http` nella folder dei reports.
Per funzionare devo installare l'estensione REST Client di vscode.

_request.http_

```http
POST http://localhost:3000/reports
content-type: application/json

{

}
```

Se proviamo ad inviarla riceviamo un messaggio di errore per ogni campo che non abbiamo compilato:

```http
HTTP/1.1 400 Bad Request
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 603
ETag: W/"25b-ub03/BYJrkd+YpIipRGt5nZ7S7Y"
Date: Thu, 15 Sep 2022 07:12:10 GMT
Connection: close

{
  "statusCode": 400,
  "message": [
    "make must be a string",
    "model must be a string",
    "year must not be greater than 2050",
    "year must not be less than 1900",
    "year must be a number conforming to the specified constraints",
    "mileage must not be greater than 1000000",
    "mileage must not be less than 0",
    "mileage must be a number conforming to the specified constraints",
    "lng must be a longitude string or number",
    "lat must be a latitude string or number",
    "price must not be greater than 1000000",
    "price must not be less than 0",
    "price must be a number conforming to the specified constraints"
  ],
  "error": "Bad Request"
}
```

Infatti, nel nostro DTO, nessuna property ha il campo `@IsOptional()`.

Quindi:

_requests.http_

```http
POST http://localhost:3000/reports
content-type: application/json

{
  "make": "toyota",
  "model": "corolla",
  "year": 1980,
  "mileage": 230000,
  "lng": 0,
  "lat": 0,
  "price": 5400
}
```

## Building associations

Le associations servono per associare, per esempio, un report ad un utente. Nella tabella dei reports abbiamo bisogno di una colonna `user_id` che contiene l'id dell'utente che ha creato il report.

## Types of associations

Le associations possono essere:

- _one-to-one_: country - capital, car - engine, passport - person, person - cell phone. es: ogni capitale appartiene ad un solo paese e ogni paese ha una sola capitale.
- _one-to-many / many-to-one_: customers - orders, car - parts, country - cities, continet - mountains. es: un paese ha tante città ma ogni città ha un solo paese.
- _many-to-many_: trains - riders, classes - students, parties - students, album - genre. es: una lezione può avere molti studenti e ogni studente può frequentare molte lezioni.

Nel nostro caso abbiamo una relazione _one-to-many / many-to-one_

## The many-to-one and one-to-many decorators

Nell'entità dell'utente:

_user.entity.ts_

```ts
@OneToMany(() => Report, (report) => report.user)
reports: Report[];
```

E in quella dei report:

_report.entity.ts_

```ts
@ManyToOne(() => User, (user) => user.reports)
user;
```

Il decorator `OneToMany` non causa nessun cambiamento nel DB, mentre il decorator `ManyToOne` aggiunge una colonna con l'informazione di come è associato (es `user_id`).

## More on decorators

Di default se faccio il fetch di un utente non recupero anche i suoi reports associati. La stessa cosa vale anche al contrario. Se faccio il fetch di un report non ottengo l'istanza dell'utente che lo ha creato.

Il primo argomento di `@OneToMany` è `() => Report` e ci dice che un utente è associato ai reports. Si usa una funzione perché altrimenti si avrebbe una circular dependency e l'entità il cui codice viene eseguito per prima non avrebbe le informazioni dell'altra (sarebbe quindi `undefined`). È un problema che hanno molti linguaggi e si risolve mettendoli in una funzione. In questo modo, verranno valutante nel futuro quando serviranno.

Il secondo argomento `(report) => report.user` ci dice quale property dell'entità è collegata alla prima.

## Setting up the association

Per salvare un report ho bisogno di tutta l'instanza di un utente. Quindi:

_reports.controller.ts_

```ts
@Post()
createReport(@Body() body: CreateReportDto, @CurrentUser() user: User) {
  return this.reportsService.create(body, user);
}
```

_reports.service.ts_

```ts
create(reportDto: CreateReportDto, user: User) {
  const report = this.repo.create(reportDto);
  report.user = user;
  return this.repo.save(report);
}
```

Così facendo, però, quando creo un nuovo utente il server mi risponde con anche la password dell'utente:

```http
HTTP/1.1 201 Created
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 236
ETag: W/"ec-+lRgMmXsuRcIJwBVjGXeFpU2LqA"
Date: Fri, 16 Sep 2022 03:53:07 GMT
Connection: close

{
  "price": 5400,
  "make": "toyota",
  "model": "corolla",
  "year": 1980,
  "lng": 0,
  "lat": 0,
  "mileage": 230000,
  "user": {
    "id": 1,
    "email": "user1@test.com",
    "password": "7c8fcbd7bfc7ed18.2e21cb80c485c49dc3ce72e9aed1e62e5f0ee54b939a006cab636a520bc12ef1"
  },
  "id": 2
}
```

## Formatting the response

Abbiamo bisogno di un serializer per togliere la password dalla response. Abbiamo già un interceptor di serializzazione (_serialize.interceptor.ts_). Spesso un pattern è quello di nascondere tutte le associations e lasciare solo i loro id nella response.

## Transforming properties with DTO

Creiamo un nuovo DTO per formattare i report in `/reports/dtos`:

_report.dto.ts_

```ts
import { Expose, Transform } from 'class-transformer';

export class ReportDto {
  @Expose()
  id: number;

  @Expose()
  price: number;

  @Expose()
  make: string;

  @Expose()
  model: string;

  @Expose()
  year: number;

  @Expose()
  lng: number;

  @Expose()
  lat: number;

  @Expose()
  mileage: number;

  // prendi l'intera entità e tira fuori user.id
  @Transform(({ obj }) => obj.user.id)
  @Expose()
  userId: number;
}
```

Quindi applico la serializzazione nel route handler:

_repors.controller.ts_

```ts
@Post()
@Serialize(ReportDto)
createReport(@Body() body: CreateReportDto, @CurrentUser() user: User) {
  return this.reportsService.create(body, user);
}
```

# A basic permissions system

## Adding in report approval

Aggiungo una colonna `approved` con valore di default `false` all'entità dei report:

_report.entity.ts_

```ts
@Column({ default: false })
approved: boolean;
```

E creo una route per modificare questa proprietà:

_reports.controller.ts_

```ts
@Patch('/:id')
approveReport(@Param('id') id: string, @Body() body: ApproveReportDto) {}
```

Creo quindi `ApproveReportDto`:

_approve-report.dto.ts_

```ts
import { IsBoolean } from 'class-validator';

export class ApproveReportDto {
  @IsBoolean()
  approved: boolean;
}
```

## Testing report approval

Creo una funzione per cambiare questa proprietà dentro il `ReportsService`:

_reports.service.ts_

```ts
async changeApproval(id: number, approved: boolean) {
  const report = await this.repo.findOne({ where: { id } });
  if (!report) {
    throw new NotFoundException('report not found');
  }

  report.approved = approved;
  return this.repo.save(report);
}
```

Quindi modifico la funzione del controller aggiungendo la chiamata:

_reports.controller.ts_

```ts
@Patch('/:id')
approveReport(@Param('id') id: string, @Body() body: ApproveReportDto) {
  return this.reportsService.changeApproval(+id, body.approved);
}
```

Infine, espongo la proprietà approved nel DTO:

_report.dto.ts_

```ts
@Expose()
approved: boolean;
```

Ora posso testare con una nuova request:

_reports/requests.http_

```http
### approve an existing report
PATCH http://localhost:3000/reports/8
content-type: application/json

{
  "approved": true
}
```

Inviandola si ottiene la modifica di `approved`:

```http
HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 116
ETag: W/"74-ePhQOLL3wyPr68qwBDzx2b8xKAA"
Date: Tue, 20 Sep 2022 06:54:21 GMT
Connection: close

{
  "id": 8,
  "approved": true,
  "price": 5400,
  "make": "toyota",
  "model": "corolla",
  "year": 1980,
  "lng": 0,
  "lat": 0,
  "mileage": 230000
}
```

## Authorization vs authentication

Dobbiamo fare in modo che solo gli admin possano modificare la proprietà `approved`. L'authentication serve a capire _chi sta facendo la richiesta_, mentre l'authorization serve a capire se _la persona che sta facendo la richiesta è autorizzata a farla_. Dovremo quindi fare una nuova guard per gli admin.

## Adding an authorization guard

Aggiorno l'entità degli utenti con un campo che dice se sono admin:

_user.entity.ts_

```ts
// per ora metto default false, altrimenti typeorm si incazza perché
// ci sono utenti nel DB che non hanno questa proprietà (per ora tutti)
@Column({ default: true })
admin: boolean;
```

Quindi creo la nuova guard dentro la cartella `/src/guards`:

_admin.guard.ts_

```ts
import { CanActivate, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';

export class AdminGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();

    if (!request.currentUser) {
      return false;
    }

    return request.currentUser.admin;
  }
}
```

## The guard dosen't work?

Aggiungo la guardia alla route che voglio proteggere:

_reports.controller.ts_

```ts
@Patch('/:id')
@UseGuards(AdminGuard)
approveReport(@Param('id') id: string, @Body() body: ApproveReportDto) {
  return this.reportsService.changeApproval(+id, body.approved);
}
```

Se invio la richiesta di modificare un report, però, ottengo `403 Forbidden` come risposta. Perché?

## Middlewares, guards and interceptors

Quando una request arriva, viene prima processata dai middlewares (cookie-session è un erempio), poi dalle guards (es. `AdminGuard`), poi dagli interceptors (es `CurrentUserInterceptor`), quindi dal route handler del controller e, infine, nuovamente dagli interceptors. Il problema è quindi che l'interceptor che mette il current user nella sessione viene eseguito dopo la guard e quindi la guard non trova la proprietà `admin` nel current user.

La soluzione è quella di trasformare `CurrentUserInterceptor` in un middleware, di modo che venga eseguito prima di `AdminGuard`.

## Assigning current user with a middleware

Creo un nuovo file: `middleware/current-user.middleware.ts`:

_current-user.middleware.ts_

```ts
import { NestMiddleware } from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';
import { UsersService } from '../users.service';

@Injectable()
export class CUrrentUserMiddleware implements NestMiddleware {
  constructor(private usersService: UsersService) {}

  async use(req: Request, res: Response, next: NextFunction) {
    const { userId } = req.session || {};
    if (userId) {
      const user = await this.usersService.findOne(userId);
      // @ts-ignore
      req.currentUser = user;
    }

    next();
  }
}
```

Quindi modifico anche il module:

_users.module.ts_

```ts
import { MiddlewareConsumer, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from './auth.service';
import { User } from './user.entity';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { CUrrentUserMiddleware } from './middlewares/current-user.middleware';

@Module({
  imports: [TypeOrmModule.forFeature([User])],
  controllers: [UsersController],
  providers: [UsersService, AuthService],
})
export class UsersModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(CUrrentUserMiddleware).forRoutes('*');
  }
}
```

Ora è possibile utilizzare correttamente l'endpoint per l'approve dei report.

## Fixing a type definition error

Nel middleware, lo statement `req.currentUser = user;` non piace a TypeScript. Questo perchè `req` è di tipo `Request` che non contiene il campo `currentUser`. Quindi diciamo a TypeScript di andare a cercare `Express` nel namespace globale e gli modifichiamo l'interfaccia di `Request`:

_current-user.middleware.ts_

```ts
declare global {
  namespace Express {
    interface Request {
      currentUser?: User;
    }
  }
}
```

## Validating query string values

Creiamo un novo route handler `/reports`. Iniziamo creando un DTO di validazione:

_get-estimate.dto.ts_

```ts
import {
  IsLatitude,
  IsLongitude,
  IsNumber,
  IsString,
  Max,
  Min,
} from 'class-validator';

export class GetEstimateDto {
  @IsString()
  make: string;

  @IsString()
  model: string;

  @IsNumber()
  @Min(1900)
  @Max(2050)
  year: number;

  @IsNumber()
  @Min(0)
  @Max(1000000)
  mileage: number;

  @IsLongitude()
  lng: number;

  @IsLatitude()
  lat: number;
}
```

Quindi creiamo il route handler:

_reports.controller.ts_

```ts
@Get('')
async getEstimate(@Query() query: GetEstimateDto) {}
```

E la nuova richiesta HTTP:

_reports/requests.http_

```http
### get an estimate for an existing veichle
GET http://localhost:3000/reports?make=toyota&model=corolla&lng=0&lat=0&mileage=230000&year=1980
```

Se inviamo questa richiesta otteniamo la seguente risposta:

```http
HTTP/1.1 400 Bad Request
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 331
ETag: W/"14b-r4WufnEUNOtUdX8wOW7CyLqgx2Q"
Date: Wed, 28 Sep 2022 04:32:11 GMT
Connection: close

{
  "statusCode": 400,
  "message": [
    "year must not be greater than 2050",
    "year must not be less than 1900",
    "year must be a number conforming to the specified constraints",
    "mileage must not be greater than 1000000",
    "mileage must not be less than 0",
    "mileage must be a number conforming to the specified constraints"
  ],
  "error": "Bad Request"
}
```

Che ci sta dicendo che i parametri in input non sono compatibili con il DTO `GetEstimateDto`. Questo perché ogni volta che passiamo dei query params questi vengono trattati come stringa, mentre nel DTO richiedo che siano dei numeri! L'unica cosa che funziona è `@IsLatitude()` e `@IsLongitude()` perché questi decorator funzionano anche con le stringhe. Il problema è che la dichiarazione di tipo sottostante (`lat: number`) è sbagliata, perché ho delle stringhe. Quindi ho bisogno di parsare tutti i miei query params con il tipo desiderato.

## Transforming query string data

Per fare questo parsing uso il decorator `@Transform()`:

_get-estimate.dto.ts_

```ts
import { Transform } from 'class-transformer';
import {
  IsLatitude,
  IsLongitude,
  IsNumber,
  IsString,
  Max,
  Min,
} from 'class-validator';

export class GetEstimateDto {
  @IsString()
  make: string;

  @IsString()
  model: string;

  @Transform(({ value }) => +value)
  @IsNumber()
  @Min(1900)
  @Max(2050)
  year: number;

  @Transform(({ value }) => +value)
  @IsNumber()
  @Min(0)
  @Max(1000000)
  mileage: number;

  @Transform(({ value }) => +value)
  @IsLongitude()
  lng: number;

  @Transform(({ value }) => +value)
  @IsLatitude()
  lat: number;
}
```

Funziona!

## How will we generate an estimate

Vogliamo che `make` e `model` coincidano esattamente con i query params, `lng` e `lat` +- 5 gradi, `year` +-3 anni e ordinati per `mileage` più simile a quello richiesto. Torneremo i 3 report più simili.

# Query builders with TypeORM

## Creating a query builder

Per creare query complesse bisogna usare il metodo `createQueryBuilder`. Iniziamo dal controller:

_reports.controller.ts_

```ts
@Get('')
async getEstimate(@Query() query: GetEstimateDto) {
  return this.reportsService.createEstimate(query);
}
```

Quindi creiamo lo scheletro del metodo `createEstimate` nel servizio:

_reports.service.ts_

```ts
createEstimate({ make, model, year, mileage, lng, lat }: GetEstimateDto) {
  return this.repo.createQueryBuilder().select('*').getRawMany();
}
```

## Writing query to produce the estimate

Nella clausola `where` è importante proteggersi dalla SQL injection utilizzando i parametri in input come variabili: `where('make = :make', { make })`.

_reports.service.ts_

```ts
async createEstimate({
  make,
  model,
  year,
  mileage,
  lng,
  lat,
}: GetEstimateDto) {
  return await this.repo
      .createQueryBuilder()
      .select('AVG(price)', 'price')
      .where('make = :make', { make })
      .andWhere('model = :model', { model })
      .andWhere('lng - :lng BETWEEN -5 and 5', { lng })
      .andWhere('lat - :lat BETWEEN -5 and 5', { lat })
      .andWhere('year - :year BETWEEN -3 and 3', { year })
      .orderBy('ABS(mileage - :mileage)', 'DESC')
      .setParameters({ mileage })
      .limit(3)
      .getRawOne();
}
```

Il motivo per cui abbiamo dovuto usare `setParameters` è che `orderBy` non accetta parametri.

## Testing the estimate logic

Mi manca da aggiungere `.andWhere('approved IS TRUE')` nella query. Se lo faccio ottengo come risposta `{ "price": null }` finché non le approvo.

# Production Deployment

## Path to production

In produzione vogliamo usare postgreSQL e utilizzare delle variabile d'ambiente per la connessione del DB. Inoltre, per `cookie-session` abbiamo usato una chiave per criptare i cookie schiantata come stringa. Vogliamo portare anche questa nelle variabili d'ambiente.

## Providing the cookie key

Aggiungiamo la chiave nelle variabili d'ambiente:

_env.development_ e _env.test_

```env
COOKIE_KEY=akdsjfhhbajkf
```

Per settare questa chiave nell' `AppModule` devo aggiungere un costruttore:

_app.module.ts_

```ts
[...]
export class AppModule {
  constructor(private configService: ConfigService) {}

  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(
        cookieSession({
          // stringa usata per criptare i cookie
          keys: [this.configService.get('COOKIE_KEY')],
        }),
      )
      .forRoutes('*'); // per tutte le routes
  }
}
```

## Understanding the synchronize flag

Nell'`AppModule` abbiamo l'import di `TypeOrmModule` che è configurato con una propietà `synchronize: true`. Questa proprietà è molto importante.

Ogni volta che l'applicazione parte, TypeORM guarda le entità e le confronta con le colonne del DB e si assicura che siano identiche. Quindi, se eliminiamo una colonna dall'entità, TypeORM elimina automaticamente la colonna dalla tabella per noi. Allo stesso modo, se aggiungiamo una proprietà all'entity, TypeORM la aggiunge automaticamente alla tabella. La chiave `synchronize` serve proprio a questo: a dire a TypeORM di fare tutti questi sbattimenti per noi.

## The dangers of synchronize

Il problema di questa scelta è che se eliminiamo per sbaglio una propietà dall'entity, TypeORM ci elimina tutti i dati di quella colonna nella tabella. Quindi di default è meglio non utilizzare `synchronize: true` in produzione. Per modificare le tabelle di produzione è meglio utilizzare le migrations.

## The theory behind migrations

Ogni file di migrazione contiene due funzioni: `up()` e `down()`, `up()` dice le modifiche che devono essere fatte al DB mentre `down()` ci dice come annullarle. Per esempio, in `up()` possiamo aggiungere una tabella di utenti con `email` e `password` mentre in `down()` vogliamo droppare la tabella. Vogliamo fare una migration per ogni modifica al DB.

## Headaches with config management

Se volessimo usare le migrations in dev, dobbiamo:

- Stoppare il server
- Usare la CLI di TypeORM per generare un file vuoto di migrazione
- Aggiungere nella migration il codice per cambiare il DB
- Usare la CLI di TypeORM per applicare la migration
- Far ripartire il server

Questo diventa complicato in test e, soprattutto, in produzione.

## TypeORM and Nest config is great

Tutti i setting di connessione al DB sono dentro nell'`AppModule` a cui TypeORM però non ha accesso. Quindi devo trovare un modo di condividere queste informazioni di modo che siano accessibili da entrambi in tutti gli ambienti.
Ci sono vari modi per configurare TypeORM (json, js, ts, env, xml, yml) in modo che sappia tutto quello che gli serve sapere sul DB.
Dobbiamo selezionare uno di questi modi di modo che funzioni sia in dev, che in test che con la TypeORM CLI. Attualmente, quando l'applicazione parte, prendiamo le opzioni di connessione e le diamo a TypeORM con `TypeOrmModule.ForRootAsync()`. Il problema è che questo non funziona con la TypeORM CLI. La verità è che non esiste un metodo migliore degli altri per configurare il tutto in maniera semplice. Utilizzeremo un file javascript perché tutti gli altri metodi sono un incubo.

## Env-specific database config

Nell' `AppModule` cambio come viene configurato TypeORM:

_app.module.ts_

```ts
[...]
const dbConfig = require('../ormconfig.js');

[...]
TypeOrmModule.forRoot(dbConfig)
[...]
```

Quindi creo il file `ormconfig.js` nella root folder del progetto:

```js
var dbConfig = {
  synchronize: false,
};

switch (process.env.NODE_ENV) {
  case 'development':
    Object.assign(dbConfig, {
      type: 'sqlite',
      database: 'db.sqlite',
      entities: ['**/*.entity.js'],
    });
    break;
  case 'test':
    Object.assign(dbConfig, {
      type: 'sqlite',
      database: 'db.test.sqlite',
      entities: ['**/*.entity.ts'],
    });
    break;
  case 'production':
    // Object.assign(dbConfig, {
    //   type: 'sqlite',
    //   database: 'db.sqlite',
    //   entities: ['**/*.entity.js'],
    // });
    break;
  default:
    throw new Error('unknown environment');
}

module.exports = dbConfig;
```

Se faccio partire il server, funziona tutto finché non faccio una request che tocchi il DB. Questo perché abbiamo disabilitato il `synchronize` e quindi le tabelle non sono aggiornate.

## Insatlling the TypeORM CLI

Per installare la TypeORM CLI bisogna installare `ts-node` (che Nest ha built in) ed aggiungere la seguente riga al `package.json`: `"typeorm": "cross-env NODE_ENV=development node --require ts-node/register ./node_modules/typeorm/cli.js"`.

## Generating and running migrations

Aggiornare le configurazioni di TypeORM:

_ormconfig.js_

```js
var dbConfig = {
  synchronize: false,
  migrations: ['migrations/*.js'],
  cli: {
    migrationsDir: 'migrations',
  },
};

[...]
```

# NB

#############################
MANCANO LE ULTIME LEZIONI PERCHÉ IL MATERIALE È VECCHIO
E NON FUNZIONA NULLA DI QUELLO CHE DICE
#############################
