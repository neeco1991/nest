import { User } from 'src/users/user.entity';
import { Column, Entity, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';

@Entity()
export class Report {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: false })
  approved: boolean;

  @Column()
  price: number;

  // company who makes the veichle
  @Column()
  make: string;

  @Column()
  model: string;

  @Column()
  year: number;

  // longitude
  @Column()
  lng: number;

  // latitude
  @Column()
  lat: number;

  @Column()
  mileage: number;

  @ManyToOne(() => User, (user) => user.reports)
  user;
}
