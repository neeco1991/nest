import { Test } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { User } from './user.entity';
import { UsersService } from './users.service';

describe('AuthService', () => {
  let service: AuthService;
  let fakeUserService: Partial<UsersService>;

  beforeEach(async () => {
    // create a fake copy of the user service
    const users: User[] = [];
    fakeUserService = {
      find: (email: string) => {
        const filteredUsers = users.filter((user) => user.email === email);
        return Promise.resolve(filteredUsers);
      },
      create: (email: string, password: string) => {
        const user = {
          email,
          password,
          id: Math.floor(Math.random() * 999999),
        } as User;
        users.push(user);
        return Promise.resolve(user);
      },
    };

    // creo il DI container
    const module = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: UsersService,
          useValue: fakeUserService,
        },
      ],
    }).compile();

    service = module.get(AuthService);
  });

  it('can create an instance of auth service', async () => {
    expect(service).toBeDefined();
  });

  it('creates a new users with a salted and hashed password', async () => {
    const password = 'testtest';
    const user = await service.signup('test@test.com', password);
    expect(user.password).not.toEqual(password);

    const [salt, hash] = user.password.split('.');
    expect(salt).toBeDefined();
    expect(hash).toBeDefined();
  });

  it('throws an error if user signs up with email that is in use', async () => {
    await service.signup('test@test.com', 'testtest');

    const userPromise = service.signup('test@test.com', 'testtest');
    await expect(userPromise).rejects.toThrow('Email already exists');
  });

  it('throws if signin is called with an unused email', async () => {
    const userPromise = service.signin('test@test.conm', 'testest');
    await expect(userPromise).rejects.toThrow('User not found');
  });

  it('throws if invalid password is provided', async () => {
    await service.signup('test@test.com', 'test');

    const userPromise = service.signin('test@test.com', 'not-test');
    await expect(userPromise).rejects.toThrow('Bad password');
  });

  it('returns an user if correct password is provided', async () => {
    await service.signup('test2@test.com', 'test');

    const user = await service.signin('test2@test.com', 'test');
    expect(user).toBeDefined();
  });
});
